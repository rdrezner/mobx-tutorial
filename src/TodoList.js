import React from 'react'
import { observer } from 'mobx-react'

@observer
export default class TodoList extends React.Component {

    createNew(e) {
        if(e.which === 13) {
            this.props.store.createTodo(e.target.value)
            e.target.value = ''
        }
    }

    toggleComplete(todo) {
        todo.completed = !todo.completed;
    }

    filter(e) {
        this.props.store.filter = e.target.value
    }

    render() {
        const {clearComplete, filter, filteredTodos} = this.props.store
        const todoLis = filteredTodos.map((todo) => {
            return <li key={todo.id}>
                <input type="checkbox" value={todo.completed} checked={todo.completed} onChange={this.toggleComplete.bind(this, todo)}/>{todo.value}
            </li>
        })
        return (
            <div>
                <h1>Todos</h1>
                <input className="create" onKeyPress={this.createNew.bind(this)}/>
                <input className="filter" value={filter} onChange={this.filter.bind(this)}/>
                <ul>{todoLis}</ul>
                <a href='#' onClick={clearComplete}>Clear Complete</a>
            </div>
        )
    }
}