import { autorun, observable, computed } from 'mobx'

class Todo {
    @observable value
    @observable id
    @observable completed

    constructor(value) {
        this.value = value
        this.id = Date.now()
        this.completed = false
    }
}

class TodoStore {
    @observable todos = []
    @observable filter = ''
    @computed get filteredTodos() {
        const matchesFilter = new RegExp(this.filter, "i")
        return this.todos.filter((todo) => !this.filter || matchesFilter.test(todo.value))
    }

    createTodo(value) {
        this.todos = [...this.todos, new Todo(value)]
    }

    clearComplete = () => {
        const incompleteTodos = this.todos.filter((todo) => !todo.completed)
        this.todos.replace(incompleteTodos);
    }
}

var store = window.store = new TodoStore()

export default store

autorun(() => {
    console.log(store.filter)
    console.log(store.todos[0])
})